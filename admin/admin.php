<?php 
	session_start();
	if (!isset($_SESSION["loket_client"])) {
		$_SESSION["loket_client"] = NULL;
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Puskesmas Narumonda</title>
	    <link href="../assert/css/bootstrap.min.css" rel="stylesheet">
	    <!-- <link href="../assert/css/jumbotron-narrow.css" rel="stylesheet"> -->
		<script src="../assert/js/jquery.min.js"></script>
	</head>
  	<body>
	<nav class="navbar navbar-default">
	  	<div class="container-fluid">
		    <div class="navbar-header" style="font-weight: bold">
		      	<a class="navbar-brand" href="index.php">Puskesmas Narumonda</a>
		    </div>
		    <ul class="nav navbar-nav" style="float: right; font-weight: bold">
				<li><a href="../index.php">Beranda</a></li>
				<li><a href="../client/index.php">Antrian Pasien</a></li>
				<li><a href="../server/index.php">Monitoring</a></li>
				<li><a href="../admin/admin.php">Admin</a></li>
		    </ul>
	  	</div>
	</nav>
    <div class="container">
    	<div class="row" style="text-align: center;">
    	<div class="col-md-3" style="text-align: center; display:inline-block; float:none;">
    		<form>
	    		<div class="jumbotron">
		        <h1 class="counter">
		        	<span class="glyphicon glyphicon-home"></span>
		        </h1>
		      	</div>
	        	<label for="exampleInputEmail1">Jumlah Loket</label>
	    		<div class="alert alert-info alert-dismissible peringatan" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Info !</strong> Jumlah loket berhasil disave
				</div>
	        	<input type="text" class="form-control loket" placeholder="Jumlah Loket">
	        	<br/>
	        	<label for="exampleInputEmail1">Ulang Antrian Mulai Dari 0</label>
	        	<div class="reset_status">
				</div> 
	        	<button type="button" class="btn btn-primary reset">Ulang</button>
				<br/>
	    	</form>
    	</div>
    	</div>
    	<br/>
      	<footer class="footer" style="position:absolute; bottom:0; text-align: center;">
	        <p>&copy; Warnam Co. <?php echo date("Y");?></p>
      	</footer>
    </div>
  	</body>

  	<script type="text/javascript">
	$("document").ready(function()
	{
		$('.peringatan').hide();

		// GET JUMLAH LOKET
	    $.post( "../apps/admin_server.php", function( data ) {
			$(".loket").val(data['jumlah_loket']);
		},"json");
		
		// NUMBER LOKET
	    $('form input').data('val',  $('form input').val() );
	    $('form input').change(function() {
	    	//set seassion or save
	    	var data = {"jmlloket": $(".loket").val()};
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "../apps/admin_server.php",//request
				data: data,
				success: function(data) {
					if (data["status"])
					{
						$('.peringatan').show();
					}
				}
			});
	    });
	    $('form input').keyup(function() {
	        if( $('form input').val() != $('form input').data('val') ){
	            $('form input').data('val',  $('form input').val() );
	            $(this).change();
	        }
	    });

	    // RESET 
		$(".reset").click(function(){
			$.post( "../apps/admin_reset.php", function( data ) {
			var alert = '<div class="alert alert-info alert-dismissible reset_status" role="alert">'+data['status']+'</div>';
			$(".reset_status").html(alert);
			},"json");
		});

	});
	</script>
</html>

