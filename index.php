<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Puskesmas Narumonda</title>
	    <link href="assert/css/bootstrap.min.css" rel="stylesheet">
	</head>
  	<body>
  		<nav class="navbar navbar-default">
		  	<div class="container-fluid">
			    <div class="navbar-header" style="font-weight: bold">
			      	<a class="navbar-brand" href="index.php">Puskesmas Narumonda</a>
			    </div>
			    <ul class="nav navbar-nav" style="float: right; font-weight: bold">
					<li><a href="index.php">Beranda</a></li>
					<li><a href="client/index.php">Antrian Pasien</a></li>
					<li><a href="server/index.php">Monitoring</a></li>
					<li><a href="admin/admin.php">Admin</a></li>
			    </ul>
		  	</div>
		</nav>

		<div class="container">
		    <div class="jumbotron">
		    	<center>
		    		<h1>Selamat Datang <br> UPT Puskesmas Narumonda</h1>
		    		<br>
		    		<img src="assert/img/puskesmas.jpg" style="width: 100%;">
		    	</center>
		    </div>
		</div>
	</body>
</html>
